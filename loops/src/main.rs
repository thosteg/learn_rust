fn main() {

    // the rust book even suggests using loops for repeated tries of things that might fail...
    // For some reason, that felt surprising

    // Unconditional loops: loop

    let mut counter = 1;
    let multiple = loop {
        counter *= 2;

        if counter > 1000000 {
            break counter;  // the value after the "break" is returned
        }
    };

    println!("Multiple of 2: {}", multiple);

    // Conditional loops: while 

    let mut countdown = 10;

    while countdown >= 0 {
        println!("{}", countdown);
        countdown -= 1;
    }

    println!("Launch!");

    // Iterate through collection: for
    // Used most commonly because of its safety and conciseness
    
    // array
    let a = [1, 2, 3, 5, 8];
    for element in a.iter() {
        println!("value: {}", element);
    }

    // constant array
    const A : [i32; 5] = [1, 2, 3, 5, 8];
    for element in A.iter() {
        println!("value: {}", element);
    }

    // countup using range
    println!("Ich zähle bis Drei!");
    for number in 1..4 {
        println!("{}", number);
    }

    // countdown using range
    for number in (1..10).rev() {
        println!("{}", number);
    }
    println!("LIFTOFF!!!");

}
