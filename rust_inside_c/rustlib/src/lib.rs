#[no_mangle]
pub extern "C" fn sample_fn() {
    println!("from rust function");
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
