#include "stdio.h"
#include "helper.h"
#include "rust_helper.h"

int main() {
	const int I = 10;
	printf("The result of plus_one(%d) is %d!\n", I, plus_one(I));
	printf("Calling sample_fn()...");
	sample_fn();
	printf("Completed sample_fn()");
	return 0;
}
