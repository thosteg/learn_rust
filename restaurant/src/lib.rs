
// für mich: 
// modules sind wie namespaces in C++
//
// An der Wurzel wird implizit das Modul "create" erzeugt:
//
// crate
//   \___front_of_house
//          \___hosting
//          |__ serving
//
// Module definieren auch rusts "privacy boundary".
// rust: private by default
// parents can not by default see their childrens private items,
// but children can see their parents private items.

mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}

        fn seat_at_table() {}
    }

    pub mod serving {
        fn take_order() {}

        pub fn serve_order() {}

        fn take_payment() {}
    }
}

mod back_of_house {
    fn fix_incorrect_order() {
        cook_order();
        super::front_of_house::serving::serve_order();
    }

    fn cook_order() {}

    pub struct Breakfast {
        pub toast: String,
        seasonal_fruit: String,  // not public
    }

    impl Breakfast {

        // note we need a public function that creates a Breakfast
        // because at least one member is private.
        pub fn summer(toast: &str) -> Breakfast {
            Breakfast {
                toast: String::from(toast),
                seasonal_fruit: String::from("peaches")
            }
        }
    }

    // all of a public enums items are public
    pub enum Appetizer {
        Soup,
        Salad,
    }
}

pub fn eat_at_restaurant() {
    // Absolute path
    crate::front_of_house::hosting::add_to_waitlist();

    // Relative path
    front_of_house::hosting::add_to_waitlist();


    let order = back_of_house::Appetizer::Soup;


    // Order a breakfast in the summer with Rye toast
    let mut meal = back_of_house::Breakfast::summer("Rye");

    // Change our mind
    meal.toast = String::from("Wheat");

    println!("I'd like {} toast please", meal.toast);

    // Not compiling because private:
    //meal.seasonal_fruit = String::from("pears");
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
