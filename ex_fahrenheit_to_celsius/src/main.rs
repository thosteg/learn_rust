fn main() {
    let f : f32 = 100.0;
    let c = (f - 32.0) * 5.0 / 9.0;

    println!("Fahrenheit: {}°F", f);
    println!("Celsius:    {}°C", c);
}
