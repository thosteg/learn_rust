fn fib(n: i32) -> i32 {
    if n > 2 {
        fib(n - 1) + fib(n - 2)
    } else {
        n
    }
}

fn main() {
    println!("Fibonacci:");
    for number in (1..10) {
        println!("{}: {}", number, fib(number));
    }
}
