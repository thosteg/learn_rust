#[repr(C)]
struct S {
   a: u32 
}

fn print_bytes(buf: &[u8], num: usize) {
    for index in 0 .. num {
        let x: u8 = buf[index];
        print!("{:#4x}", x);
    }
    println!("\n");
}

type Uint32Array =[u8; std::mem::size_of::<u32>()];

fn main() {
    // simple number
    let x : u32 = 0x0a0b0c0d;
    println!("u32 number:");
    println!("  x =     {:#10x}",x);
    print!(  "  bytes = ");
    unsafe {
        let bytes = std::mem::transmute_copy::<u32, Uint32Array>(&x);
        print_bytes(&bytes, std::mem::size_of::<u32>());
    }

    // struct
    let s : S = S{a: x};
    println!("");
    println!("u32 number in struct:");
    println!("  s.a =       {:#10x}", s.a);
    println!("  sizeof(s) = {}", std::mem::size_of::<S>());
    print!(  "  bytes =     ");

    unsafe {
        let bytes = std::mem::transmute_copy::<S, Uint32Array>(&s);
        print_bytes(&bytes, std::mem::size_of::<S>());
    }
}

