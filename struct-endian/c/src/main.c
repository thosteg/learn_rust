#include "stdio.h"
#include "stdint.h"

struct S {
	uint32_t a;
};

void print_bytes(void* s, int num) {
	for (int i = 0; i != num; ++i) {
		uint8_t x = ((uint8_t*)s)[i];
		printf("0x%02x ",x);
	}
	printf("\n");
}

int main() {
	printf("sizeof uint32_t: %ld\n", sizeof(uint32_t));
	printf("sizeof S:        %ld\n", sizeof(struct S));

	printf("uint32_t number:\n");
	uint32_t x = 0x0a0b0c0d;
	printf("  number: 0x%x\n", x);
	printf("  bytes:  ");
	print_bytes(&x, sizeof(x));

	printf("struct S containing number:\n");
	struct S s;
	s.a = x;
	printf("  bytes:  ");
	print_bytes(&s, sizeof(s));

	return 0;
}
