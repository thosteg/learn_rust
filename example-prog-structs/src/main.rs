fn main() {
    let width1 = 7;
    let height1 = 5;
    println!("Area of rectangle: {}", area(width1, height1));

    let rect1 = (width1, height1);
    println!("Area of rectangle (using tuples): {}", area_via_tup(rect1));

    let rect2 = Rectangle {
        width: width1,
        height: height1
    };
    println!("Area of rectangle (using struct): {}", area_via_rectangle(&rect2));
    println!("Area of rectangle (using method on struct): {}", rect2.area());
    println!("Try to print rectangle using {{:?}} {:?}", rect2);
    println!("Try to print rectangle using {{:#?}} {:#?}", rect2);

    // can_hold:
    let r1 = Rectangle::rectangle(30, 50);
    let r2 = Rectangle::rectangle(10, 40);
    let r3 = Rectangle::rectangle(60, 45);

    println!("Can hold r1.can_hold(r2): {}", r1.can_hold(&r2));
    println!("Can hold r1.can_hold(r3): {}", r1.can_hold(&r3));
}

fn area(width: u32, height: u32) -> u32 {
    width * height
}

fn area_via_tup(dimensions: (u32, u32)) -> u32 {
    dimensions.0 * dimensions.1
}

#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32
}


impl Rectangle {  // implementation block
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width >= other.width && self.height >= other.height
    }

    fn rectangle(width: u32, height: u32) -> Rectangle {
        Rectangle {
            width,
            height
        }
    }
}

fn area_via_rectangle(rectangle: &Rectangle) -> u32 {
    rectangle.width * rectangle.height
}
