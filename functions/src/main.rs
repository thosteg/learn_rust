fn main() {
    println!("Hello, world!");
    another_function(7, 4.0);
}

fn return_magic() -> i32 {
    return 6;
}

fn seven() -> i32 {
    7
}

fn minus(x: i32) -> i32{
    -x
}

fn another_function(key: i32, val : f64)
{
    // Functions contain statements ...
    
    println!("{}: {}", key, val);
    let x = minus(42);  // statement
    println!("{}", x);

    //let a = (let b = 6);  // statements do NOT return values

    let d = return_magic() + seven();  // calling function is an expression
    let e = {       // scopes are expressions (!)
        let f = 9;  // statement
        f + 3       // expression
    };

    println!("Result: {}", e + {let g = d-e; g + 3});
    // .. optionally ending in an expression

}