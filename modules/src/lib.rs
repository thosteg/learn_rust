#![allow(dead_code)]
#![allow(unused_variables)]

// From https://doc.rust-lang.org/book/ch07-05-separating-modules-into-different-files.html:
// Using a semicolon after mod front_of_house rather than using a block tells 
// Rust to load the contents of the module from another file with the same name
// as the module. 
mod front_of_house;


mod back_of_house {
    pub struct Breakfast {
        pub bread: String,

        // private field needs to be instantiated by public associated method
        seasonal_fruit: String
    }

    pub enum Appetizer {
        Soup,
        Fruits
    }

    pub fn summer(bread: &str) -> Breakfast {
        Breakfast {
            bread: String::from(bread),
            seasonal_fruit: String::from("peaches")
        }
    }
}

fn fix_order() {

}

pub fn eat_at_restaurant() {
    // absolute path
    crate::front_of_house::hosting::add_to_waitlist();

    // relative path
    front_of_house::hosting::add_to_waitlist();

    // Initializing private attributes via public associated method
    let mut breakfast = back_of_house::summer("wheat");
    breakfast.bread = String::from("rye");

    println!("I'd like {} bread, please!", breakfast.bread);
    // breakfast.seasonal_fruit = String::from("kiwi");  // private

    // Public enum allows access to allow values
    let appetizer = back_of_house::Appetizer::Fruits;

    // Using "use" to bring parent module into scope
    use crate::front_of_house::hosting;
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();

    // We can also bring the final method into scope, but this is
    // slightly less clear as to what method is locally defined
    use crate::front_of_house::hosting::add_to_waitlist;
    add_to_waitlist();

    // In contrast, structs and enums are regularly brought into
    // scope with the whole path - just what established itself
    
    // use back_of_house::Appetizer;
    let a : Appetizer = Appetizer::Soup;

    // bring into scope with local alias (e.g. to avoid ambiguity)
    use back_of_house::Appetizer as UpfrontSnack;
    let b = UpfrontSnack::Fruits;
}

// Used external code is private by default.
// It can be re-exported as public code as follows.
pub use back_of_house::Appetizer;


fn usage_example() {
    #![allow(unused)]
    use std::collections::HashMap;

    // Using things in a combined way
    use std::{cmp::Ordering, array};
    use std::io::{self, Write};

    // Using the glob pattern
    // Often used for testing to bring things into the test scope
    use std::boxed::*;
}

#[test]
fn module_test() {
    eat_at_restaurant();
}
