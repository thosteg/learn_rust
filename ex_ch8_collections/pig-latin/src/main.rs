fn pig_latin(s: &str) -> String {
    let first = s.chars().nth(0).unwrap();
    let rest = s.get(1..).unwrap();
    match first.to_lowercase().to_string().chars().nth(0).unwrap() {
        'a' | 'e' | 'i' | 'o' | 'u' | 'ä' | 'ö' | 'ü' => {
            format!("{}-hay", s)
        }
        _ => {
            format!("{}-{}ay", rest, first)
        }
    }
}

#[test]
fn test_pig_latin() {
    assert_eq!(&pig_latin("first"), &"irst-fay");
    assert_eq!(&pig_latin("apple"), &"apple-hay");
}

fn main() {
    let s = "autotür";
    let t = "küche";
    println!("{} -> {}", s, pig_latin(s));
    println!("{} -> {}", t, pig_latin(t));
}
