use std::collections::HashMap;

fn median<T>(v: &[T]) -> Option<T>
where
    T: Copy,
{
    match v.len() {
        0 => None,
        a => Some(v[(a - 1) / 2]),
    }
}

fn mode(v: &[i32]) -> Option<i32> {
    match v.len() {
        0 => None,
        _ => {
            let mut hist: HashMap<i32, i32> = HashMap::<_, _>::new();
            for val in v.into_iter() {
                let cnt = hist.entry(*val).or_insert(0);
                *cnt += 1;
            }
            let mut cnt_max = 0;
            let mut m = 0; // will be replaced
            for (k, v) in hist.into_iter() {
                if v > cnt_max {
                    cnt_max = v;
                    m = k;
                }
            }
            Some(m)
        }
    }
}

#[test]
fn test_median() {
    let empty_array: [i32; 0] = [0; 0];
    assert_eq!(crate::median(&empty_array), None);
    assert_eq!(crate::median(&[1]), Some(1));
    assert_eq!(crate::median(&[7, 6]), Some(7));
    assert_eq!(crate::median(&[1, 2, 3, 4, 5, 6, 7]), Some(4));
    assert_eq!(crate::median(&[1, 2, 3, 4, 5, 6]), Some(3));
}

#[test]
fn test_mode() {
    let empty_array: [i32; 0] = [0; 0];
    assert_eq!(crate::mode(&empty_array), None);
    assert_eq!(crate::mode(&[1]), Some(1));
    assert_eq!(crate::mode(&[2, 2, 3]), Some(2));
    assert_eq!(crate::mode(&[1, 2, 3, 4, 3, 3, 2]), Some(3));
}

fn main() {
    let v = vec![5, 3, 2, 6, 7, 2, 8, 9, 2, 3];

    println!("Median of {:?} -> {}", v, median(&v).unwrap());
    println!("Mode of   {:?} -> {}", v, mode(&v).unwrap());
}
