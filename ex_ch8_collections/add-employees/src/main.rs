use std::collections::HashMap;
use std::io;

enum Action {
    Exit,
    Help,
    Add((String, String)),
    ListDepartments,
    ListDepartment(String),
    ListPeople,
}

fn extract_tokens(s: &String) -> Vec<&str> {
    let tokens: Vec<&str> = s.split_ascii_whitespace().collect();
    tokens
}

fn identify_action(s: &String) -> Action {
    let tokens = extract_tokens(s);
    match tokens.len() {
        0 => Action::Help,
        _ => match tokens[0] {
            "exit" => Action::Exit,
            "help" => Action::Help,
            "add" => {
                if tokens.len() < 4 || tokens[2] != "to" {
                    Action::Help
                } else {
                    Action::Add((String::from(tokens[1]), String::from(tokens[3])))
                }
            }
            "list" => {
                if tokens.len() < 2 {
                    Action::Help
                } else {
                    match tokens[1] {
                        "all" => Action::ListPeople,
                        "departments" => Action::ListDepartments,
                        "department" => {
                            if tokens.len() < 3 {
                                Action::Help
                            } else {
                                let department = tokens[2];
                                Action::ListDepartment(String::from(department))
                            }
                        }
                        _ => Action::Help,
                    }
                }
            }
            _ => Action::Help,
        },
    }
}

type DepartmentFolk = HashMap<String, Vec<String>>;

fn do_action(m: &mut DepartmentFolk, a: &Action) -> bool {
    match a {
        Action::Help => {
            println!("Available commands:");
            println!("  help                          ");
            println!("  add <user> to <department>    ");
            println!("  list departments              ");
            println!("  list department <department>  ");
            println!("  list all                      ");
            println!("  exit                          ");
            true
        }
        Action::Add((person, department)) => {
            println!("Adding \"{}\" to \"{}\"", person, department);
            let e = m.entry(department.to_string()).or_insert(vec![]);
            e.push(person.to_string());
            true
        }
        Action::ListDepartments => {
            println!("Available Departments:");
            for (k, _) in m.into_iter() {
                println!("\t{}", k);
            }
            true
        }
        Action::ListDepartment(department) => {
            println!("Departments: \"{}\"", department);
            match m.get(&department.to_string()) {
                None => {
                    println!("\tDepartment not found");
                }
                Some(folk) => {
                    for person in folk.into_iter() {
                        println!("\t{}", person);
                    }
                }
            }
            true
        }
        Action::ListPeople => {
            println!("All:");
            for (department, folk) in m.into_iter() {
                println!("\t{}", department);
                for person in folk.into_iter() {
                    println!("\t\t{}", person);
                }
            }
            true
        }
        Action::Exit => false,
    }
}

fn main() -> io::Result<()> {
    let mut buf = String::new();

    let mut store = DepartmentFolk::new();

    while do_action(&mut store, &identify_action(&buf)) {
        buf.clear();
        io::stdin().read_line(&mut buf)?;
    }
    Ok(())
}
