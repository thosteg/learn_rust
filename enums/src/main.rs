enum IpAddrKind {
    V4,
    V6,
}

struct IpAddrAlpha {
    kind: IpAddrKind,
    address: String,
}

enum IpAddr {
    V4(u8, u8, u8, u8),
    V6(String),
}

fn route(ip: &IpAddr) {}

fn some_examples() {
    let some_number = Some(5);  // automatically inferred type
    let some_string = Some("Hello there");

    match some_number {
        Some(val) => println!("{} ", val),
        None => {}
    }
}

#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska,
}

enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => 1,
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => {
            println!("State Quarter from State {:?}", state);
            25
        }
    }
}

fn sample_coins() {
    println!("Penny:             {}", value_in_cents(Coin::Penny));
    println!("Nickel:            {}", value_in_cents(Coin::Nickel));
    println!("Dime:              {}", value_in_cents(Coin::Dime));
    println!("Quarter(Alabama):  {}", value_in_cents(Coin::Quarter(UsState::Alabama)));
    println!("Quarter(Alaska):   {}", value_in_cents(Coin::Quarter(UsState::Alaska)));
}

fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i + 1),
    }
}

fn sample_plus_one() {
    let five = Some(5);
    let six = plus_one(five);
    let none = plus_one(None);
}

fn sample_placeholder() {
    let some_u8_value = 0u8;
    match some_u8_value {
        1 => println!("one"),
        3 => println!("three"),
        _ => (),  // unit value
    }
}

fn test_if_let() {
    let coin_a = Coin::Penny;
    let coin_b = Coin::Quarter(UsState::Alaska);

    if let Coin::Quarter(state) = coin_b {
        println!("Coin from state {:?}", state);
    } else {
        println!("Other coin");
    }
}

fn main() {
    let local_4 = IpAddr::V4(127,0,0,1);
    let local_6 = IpAddr::V6(String::from("::1"));

    route(&local_6);
    route(&local_4);

    some_examples();

    sample_coins();
    sample_plus_one();
    sample_placeholder();
    test_if_let();
}
