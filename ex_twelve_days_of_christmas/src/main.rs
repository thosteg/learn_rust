fn main() {
    let days = ["first", "second", "third", "fourth", "fifth", "sixth",
     "seventh", "eighth", "ninth", "tenth", "eleventh", "twelvth"];
    let items = [
        "a partridge in a pear tree",
        "two turtle doves",
        "three french hens",
        "four calling birds",
        "five golden rings",
        "six geese a-laying",
        "seven swans a-swimming",
        "eight maids a-milking",
        "nine ladies dancing",
        "ten lords a-leaping",
        "eleven pipers piping",
        "twelve drummers drumming"];

    for num in 0..12 {
        println!(
            "\nOn the {} day of christmas my true love sent to me: ",
            days[num]);
        for item in (0 .. num + 1).rev() {
            if item == 0 {
                if num != 0 {
                    print!(" and ");
                }
            } else {
                if item != num {
                    print!(", ");
                }
            }
            print!("{}", items[item]); 
        }
    println!(".");
    }
}
