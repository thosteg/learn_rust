#![allow(dead_code, unused_variables)]

#[allow(unused_mut)]
#[allow(unused_assignments)]
fn variables()  {
    let x = 3;  // immutable
    // x = 2;   // would not work
    let x = 2;  // shadowing variable - creates new variable, old becomes unseen
    let x = "Hi there";  // different type - works as well

    let mut y = 5.5;  // mutable
    y = 3.0;
    y = -7.0;


    const NUM_ELEMENTS: u32 = 10_000;  // constants, type required, const_expr, convention UPPER CASE

    let spaces = "      ";
    let spaces = spaces.len();  // shadow with different type

    println!("Value:  {} {}", x, y);

    // let hänsel = "Variable mit nem Sonderzeichen";  // not fully supported
}

fn types() {
    let guess: u32 = "42".parse().expect("Not a number!");
    println!("Number: {} ", guess);

    let signed: i128 = -23235462357465;
    let unsigned: u128 = 1426534654654;

    println!("Signed: {}, Unsigned: {} ", signed, unsigned);
}

fn integer_literals() {
    println!("Decimal: {} {} Hex: {} {} Octal: {} Binary: {} Byte: {}", 
            98_123, 12345, 0xff, 0x1_000_0000, 0o77, 0b1111_0000, b'A');
}

#[allow(unused_mut)]
fn overflow() {
    let mut i: i8 = 127;
    // i = i + 1;  // in debug mode, panic. When building in --release mode, no check!
    // Explicit wrapping is possible
}

fn floating_point()
{
    let x = 2.5;  // f64 (default)
    let y: f32 = 3.0;  // f32
    println!("Floating points: {} {}", x, y);
}

fn numeric_operations() {
    let sum = 4 + 5;
    let difference = 2 - 3;
    let product = 4 * 5;
    let quotient = 3.2 / 5.7;
    let remainder = 13 % 10;
}

fn boolean() {
    let t = true;
    let f: bool = false;
}

use std::mem;

fn characters() {
    // characters in single quotes, as opposed to strings

    // rust characters are 4 bytes in size, unicode scalar value

    let c = 'z';
    let z = 'ℤ';
    let heart_eyed_cat = '😻';

    println!("Some characters: {} {} {}, sizeof({}) = {}, sizeof({}) = {}", 
             c, z, heart_eyed_cat,
             c, mem::size_of_val(&c),
             heart_eyed_cat, mem::size_of_val(&heart_eyed_cat));
}

fn tuple() {
    let tup: (i32, f64, u8) = (500, 6.2, 1);  // types are optional
    let tapa = ("Hi there", 32, 7.0);
    let (s, u, d) = tapa;  // destructuring

    println!("The value of s is: {}", s);

    println!("The value of the second element of the tuple is : {}", tup.1);  // access tuple element by index
}

fn arrays() {
    // Array has a fixed length
    // Arrays are allocated on the heap
    
    let a = [-1, -2, 1, 3];
    let monate = ["Januar", "Februar", "März", "April"];

    println!("The second element in the array is : {}", a[1]);
    println!("Der dritte Monat ist: {}", monate[2]);

    let b : [i32; 3] = [-123144, 234234234, 32];  // explicitely setting the type as an array (type + size)
    println!("The second element is: {}", b[1]);

    let c = [-2; 5];  // Create an array of 5 elements, all initialized to -2
    println!("The last element in c is {}", c[4]);

    // let x = c[5];  // Will create a compile-time error

    // let index = 5;
    // let x = c[index];  // Will compile, but create a run-time error
}

fn compound_types() {
    tuple();
    arrays();
}

fn main() {
    variables();
    types();
    integer_literals();
    // Note also: Integer defaults to i32, for speed
    overflow();
    floating_point();
    numeric_operations();
    boolean();
    characters();
    compound_types();
}

