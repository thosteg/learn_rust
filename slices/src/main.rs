fn first_word(s: &str) -> &str {  // &str ist String slice type
    let bytes = s.as_bytes();
    let it = bytes.iter();
    for (i, &item) in it.enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }
    return s;
}

fn second_word(s: &str) -> &str {
    let bytes = s.as_bytes();
    let it = bytes.iter();
    let mut s1:usize = 0;
    for (i, &item) in it.enumerate() {
        if item == b' ' {
            if s1 == 0 {
                s1 = i + 1;
            } else {
                return &s[s1 .. i];
            }
        }
    }
    return s;
}

fn main() {
    let x = String::from("Mein Name ist Thomas");

    let first = &x[0..4];
    let second = &x[5..9];
    let third = &x[10..13];

    println!("{}:{}:{}:", first, second, third);

    let one = first_word(&x);
    // x.clear();  // fails on purpose
    println!("one:     {}", one);
    println!("second:  {}", second_word(&x));
    println!("second word of constant string: \"{}\"", second_word("Guten Morgen euch!"));

    let int_a = [0, 1, 2, 3, 4];
    for &i in int_a[1..4].iter() {
        println!("{}", i);
    }
}
