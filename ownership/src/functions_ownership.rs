// terms:
// - ownership (by assignment of variable)
// - reference (like pointers, but are always valid)
//
// references can be borrowed:
// - multiple read-only references: &var
// - a single mutable reference: &mut var

fn consume(s: String) {
    println!("Inside consuming function: {}", s);
}

fn transfer_to_from(s: String) -> String {
    println!("Inside function: {}", s);
    s
}

fn transfer_tuple(i: i32, s: String) -> (i32, String, usize) {
    let length = s.len();
    (i, s, length)
}

pub fn run_simple_transfers() {
    let a = String::from("Consumed String");
    println!("Before consume: {}", a);
    consume(a);
    // println!("After consume: {}", a);  // forbidden

    let b = String::from("Transfered String");
    println!("Before transfer into: {}", b);
    let c = transfer_to_from(b);
    println!("After transfer out of: {}", c);

    println!("Before transfer into: {}", c);
    let c = transfer_to_from(c);
    println!("After transfer out of: {}", c);

    println!("Before transfer into: {}", c);
    let (x, c, len) = transfer_tuple(7, c);
    println!("After transfer out of: {} {} {}", x, c, len);
}

fn immutable_reference(s: &String) -> usize {
    s.len()
}

fn mutable_reference(s: &mut String) {
    s.push_str(" append");
}

pub fn run_mutable_immutable_funcs() {
    let mut si : String = String::from("Hello there");

    // Copy of original
    let yi = si.clone();
    let len_yi = immutable_reference(&yi);

    // modify original
    mutable_reference(&mut si);
    let len_si = immutable_reference(&si);

    println!("'modified:         {}' has length: {}", si, len_si);
    println!("'copy of original: {}' has length: {}", yi, len_yi);
}

fn ownership_from_function() -> String{
    let s: String = String::from("Created inside function");
    s
}

pub fn take_ownership_out_of_function() {
    let x: String = ownership_from_function();
    println!("Variable x: {}", x);
}