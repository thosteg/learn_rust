mod str_append;
mod assignment;
mod functions_ownership;

fn main() {
    str_append::append_str();
    assignment::primitive();
    assignment::transfer();
    assignment::clone();
    functions_ownership::run_simple_transfers();
    functions_ownership::run_mutable_immutable_funcs();
    functions_ownership::take_ownership_out_of_function();
}
