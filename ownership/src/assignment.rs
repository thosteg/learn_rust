pub fn primitive() {
    let x = 7;
    let y = x;  // copy x

    println!("{}", x);  // geht!
    println!("{}", y);  // geht!
}

pub fn transfer() {
    let x = String::from("erster");  // note: x is on stack, with ptr to data on heap
    let y = x;                       // copy elements in x, move ownership of ptr (in heap)

    // println!("{}", x);  // geht nicht!
    println!("{}", y);  // geht!

    // when y goes out of scope, it will free the heap memory refered to by ptr
    // when x goes out of scope, it does not free anything, as no ownership (no double free)

    // note also: rust never does any "deep" copies by default, only "shallow" copies.
    // this means also automatic copies are cheap.
}

pub fn clone() {
    let s1 = String::from("tiefe Kopie");
    let s2 = s1.clone();

    println!("s1 = {}, s2 = {}", s1, s2);
}
