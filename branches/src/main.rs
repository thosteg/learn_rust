fn main() {

    // if ... then ... else
    let number = 77;

    if number % 11 == 0 && number < 100  {
        println!("schnappszahl");
    } else if number == 12 {
        println!("Zahl der Vollkommenheit");
    } else {
        println!("second");
    }

    // if in "let" statements
    //
    // Remember: if is an expression
    // All branches must have same type

    let even_odd = if (number % 2 == 0) {
        "even"
    } else {
        "odd"
        // 7 // would not work
    };

    println!("The number {} is {}", number, even_odd);
}
