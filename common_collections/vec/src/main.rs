fn main() {
    let mut v : Vec<i32> = Vec::new();
    v.push(1);
    v.push(1);
    v.push(2);
    println!("{:?}",v);

    let x = vec![1,2,3,4];
    println!("{:?}",x );

    print!("[");
    for i in &x {
        print!(" {:?}, ", i);
    }
    print!("]\n");

    for i in &mut v {
        *i += 2;
    }
    println!("{:?}",v);

    println!("The second element in x is: {:?}", x[1]);
    
    let n = 6;
    println!("Getting the {}th element in x:", n);
    match x.get(n) {
        Some(i) => {
            println!("Has value {}", i);
        }
        None => {
            println!("Does not exist");
        }
    }

    #[derive(Debug)]
    enum SpreadsheetCell {
        Int(i32),
        Float(f64),
        Text(String)
    }

    let table = vec![
        SpreadsheetCell::Int(47),
        SpreadsheetCell::Float(3.14),
        SpreadsheetCell::Text(String::from("Hello World"))
    ];

    println!("Table: {:?}", table[2]);
}
