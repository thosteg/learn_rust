struct TeliI16 {
    value: i16,
    status: u8,
    timestamp: f64,
}

fn print_i16(teli: &TeliI16)  // copy struct
{
    println!("\n\tvalue:{}\n\tstatus:{}\n\ttimestamp:{}\n", teli.value, teli.status, teli.timestamp);
}

fn build_teli_i16(value: i16, timestamp: f64) -> TeliI16 {
    TeliI16 {
        value,  // field init shorthand - if param name == struct field name
        status: 0,
        timestamp,
    }
}

fn main() {
    // regular structs
    let mut teli = TeliI16 {
        value: 7,
        status: 1,
        timestamp: 12345.0,
    };

    teli.value = 5;
    print_i16(&teli);

    let tel2 = build_teli_i16(3, 123456.0);
    print_i16(&tel2);

    // struct update syntax: create struct from other
    let tel3 = TeliI16 {
        value: 19,
        ..tel2
    };
    print_i16(&tel3);

    // tuple structs (structs without names, ordered, like tuples, but typed)
    
    struct Color(i32, i32, i32);
    let cyan = Color(0, 255, 255);
    println!("Cyan has blue component: {}", cyan.2);
}
